package main

import (
	"gitlab.com/navenest/scavenger-hunt/internal/ingest"
	"gitlab.com/navenest/scavenger-hunt/pkg/database/pg"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	"gitlab.com/navenest/scavenger-hunt/web"
	"go.uber.org/zap"
	"os"
)

func main() {
	logz.Logger.Info("logger construction succeeded")
	err := pg.DB.Ping()
	if err != nil {
		logz.Logger.Error("Unable to ping DB",
			zap.Error(err))
	}

	var path string
	if value, ok := os.LookupEnv("INGEST_FILE"); ok {
		path = value
	} else {
		path = "/app/hunt.json"
	}
	ingest.FileIngest(path)
	webserver := web.App{}
	webserver.Initialize()
	webserver.Run(":8080")
	err = pg.DB.Close()
	if err != nil {
		logz.Logger.Error("Unable to Close DB Connection",
			zap.Error(err))
	}
}
