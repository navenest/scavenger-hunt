package pg

import (
	"database/sql"
	"fmt"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

var DB *sql.DB

func initPGDB() *sql.DB {
	var host string
	if value, ok := os.LookupEnv("DB_HOST"); ok {
		host = value
	} else {
		host = "postgres"
	}
	var port int
	if value, ok := os.LookupEnv("DB_PORT"); ok {
		port, _ = strconv.Atoi(value)
	} else {
		port = 5432
	}
	var user string
	if value, ok := os.LookupEnv("DB_USER"); ok {
		user = value
	} else {
		user = "skippy"
	}
	var password string
	if value, ok := os.LookupEnv("DB_PASS"); ok {
		password = value
	} else {
		password = "skippy123"
	}
	var dbname string
	if value, ok := os.LookupEnv("DB_NAME"); ok {
		dbname = value
	} else {
		dbname = "scavenger_hunt"
	}
	var migrationsDir string
	if value, ok := os.LookupEnv("MIGRATIONS_DIR"); ok {
		migrationsDir = value
	} else {
		migrationsDir = "/usr/local/go/src/scavenger-hunt/migrations"
	}

	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		logz.Logger.Error("Unable to connect to DB",
			zap.Error(err),
			zap.String("host", host))
	}

	err = db.Ping()
	if err != nil {
		logz.Logger.Error("Unable to ping DB",
			zap.Error(err),
			zap.String("host", host))
	}

	// Create public schema
	res, err := db.Exec(`CREATE SCHEMA IF NOT EXISTS public`)
	if err != nil {
		logz.Logger.Error("Unable to create Schema",
			zap.Error(err))
	}
	rows, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from schema create",
		zap.Int64("rows", rows))

	// Create user Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS users (
    	USER_ID TEXT PRIMARY KEY NOT NULL,
    	CURRENT_HUNT INT, 
    	CURRENT_QUESTION INT
        )`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	// Create hunt Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS hunts(
    	HUNT_ID INT PRIMARY KEY NOT NULL,
    	TITLE TEXT NOT NULL,
        LATITUDE FLOAT,
	    LONGITUDE FLOAT
		);
		CREATE UNIQUE INDEX IF NOT EXISTS title_index on hunts(title)`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	logz.Logger.Info("DB Response",
		zap.Any("output", res))
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	// Create challenge_info Table
	//res, err = db.Exec(`CREATE TABLE IF NOT EXISTS challenge_info(
	//	CHALLENGE_ID INT PRIMARY KEY NOT NULL,
	//	HUNT_ID INT NOT NULL,
	//	FOREIGN KEY (HUNT_ID) REFERENCES hunts(HUNT_ID)
	//	)`)
	//if err != nil {
	//	logz.Logger.Error("Unable to create Tables",
	//		zap.Error(err))
	//}
	//rows, err = res.RowsAffected()
	//if err != nil {
	//	logz.Logger.Error("Unable to display Rows",
	//		zap.Error(err))
	//}
	//logz.Logger.Info("Rows from table create",
	//	zap.Int64("rows", rows))

	// Create challenge_questions Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS challenge_questions(
		CHALLENGE_QUESTION_ID INT PRIMARY KEY NOT NULL,
		HUNT_ID INT NOT NULL,
		FOREIGN KEY (HUNT_ID) REFERENCES hunts(HUNT_ID),
		TYPE TEXT NOT NULL,
		QUESTION TEXT
		);
		CREATE UNIQUE INDEX IF NOT EXISTS  challenge_question_index on challenge_questions(hunt_id,type,question)`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	// Create challenge_answers Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS challenge_answers(
	    CHALLENGE_ANSWERS_ID INT PRIMARY KEY NOT NULL,
	    CHALLENGE_QUESTION_ID INT NOT NULL,
	    FOREIGN KEY (CHALLENGE_QUESTION_ID) REFERENCES challenge_questions(CHALLENGE_QUESTION_ID),
	    HUNT_ID INT NOT NULL,
	    FOREIGN KEY (HUNT_ID) REFERENCES hunts(HUNT_ID),
	    LATITUDE FLOAT,
	    LONGITUDE FLOAT,
	    answer TEXT
		);
		CREATE UNIQUE INDEX IF NOT EXISTS  challenge_answer_index on challenge_answers(hunt_id,challenge_question_id)`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	// Create user_answers Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS user_answers(
	    USER_ID TEXT NOT NULL,
	    FOREIGN KEY (USER_ID) REFERENCES users(USER_ID),
	    CHALLENGE_QUESTION_ID INT NOT NULL,
	    FOREIGN KEY (CHALLENGE_QUESTION_ID) REFERENCES challenge_questions(CHALLENGE_QUESTION_ID),
	    CHALLENGE_ANSWERS_ID INT NOT NULL,
	    FOREIGN KEY (CHALLENGE_ANSWERS_ID) REFERENCES challenge_answers(CHALLENGE_ANSWERS_ID),
	    HUNT_ID INT NOT NULL,
		FOREIGN KEY (HUNT_ID) REFERENCES hunts(HUNT_ID),
		correct BOOL,
	    LATITUDE FLOAT,
	    LONGITUDE FLOAT,
	    answer TEXT
		);
		CREATE UNIQUE INDEX IF NOT EXISTS  user_answer_index on user_answers(user_id,hunt_id,challenge_question_id)`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	// Create challenge id
	//res, err = db.Exec(`CREATE SEQUENCE IF NOT EXISTS challenge_id OWNED BY challenge_info.challenge_id;`)
	//if err != nil {
	//	logz.Logger.Error("Unable to create sequence",
	//		zap.Error(err))
	//}
	//rows, err = res.RowsAffected()
	//if err != nil {
	//	logz.Logger.Error("Unable to display Rows",
	//		zap.Error(err))
	//}
	//logz.Logger.Info("Rows from sequence create",
	//	zap.Int64("rows", rows))

	// Create challenge question id
	res, err = db.Exec(`CREATE SEQUENCE IF NOT EXISTS  challenge_question_id OWNED BY challenge_questions.challenge_question_id;`)
	if err != nil {
		logz.Logger.Error("Unable to create sequence",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from sequence create",
		zap.Int64("rows", rows))

	// Is this even needed?
	//// Create user answer id
	//res, err = db.Exec(`CREATE SEQUENCE IF NOT EXISTS  user_answer_id OWNED BY user_answers.user_i`)
	//if err != nil {
	//	logz.Logger.Error("Unable to create sequence",
	//		zap.Error(err))
	//}
	//rows, err = res.RowsAffected()
	//if err != nil {
	//	logz.Logger.Error("Unable to display Rows",
	//		zap.Error(err))
	//}
	//logz.Logger.Info("Rows from sequence create",
	//	zap.Int64("rows", rows))

	// Create hunt id
	res, err = db.Exec(`CREATE SEQUENCE IF NOT EXISTS hunt_id OWNED BY hunts.hunt_id`)
	if err != nil {
		logz.Logger.Error("Unable to create sequence",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from sequence create",
		zap.Int64("rows", rows))

	//dir, err := os.Getwd()
	//if err != nil {
	//	logz.Logger.Error("Unable to perform migration",
	//		zap.Error(err))
	//}
	logz.Logger.Info(migrationsDir)

	//driver, err := postgres.WithInstance(db, &postgres.Config{})
	//m, err := migrate.NewWithDatabaseInstance(
	//	"file://"+migrationsDir,
	//	"postgres", driver)
	//err = m.Up()
	//if err != nil {
	//	logz.Logger.Warn("Unable to perform migration",
	//		zap.Error(err))
	//}

	db.SetMaxOpenConns(75)
	db.SetMaxIdleConns(25)
	db.SetConnMaxLifetime(1 * time.Minute)

	return db
}

func init() {
	logz.Logger.Info("Connecting to DB")
	DB = initPGDB()
	logz.Logger.Info("Connected to DB")
}
