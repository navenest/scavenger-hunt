package ingest

import (
	"encoding/json"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	"go.uber.org/zap"
	"io/ioutil"
	"testing"
)

func TestFileIngest(t *testing.T) {
	exampleFile, err := ioutil.ReadFile("../../examples/hunts/hunt.json")
	if err != nil {
		logz.Logger.Error("Unable to read json file",
			zap.Error(err))
	}

	exampleData := Hunts{}
	err2 := json.Unmarshal(exampleFile, &exampleData)
	if err2 != nil {
		logz.Logger.Error("Unable to unmarshal json file",
			zap.Error(err2))
	}

	FileIngest("../../examples/hunts/hunt.json")

	//if !cmp.Equal(data.Hunts, exampleData.Hunts) {
	//	t.Errorf("expected '%+v', got '%+v'", exampleData, data)
	//	t.Error(cmp.Diff(exampleData, data))
	//}
}

// TODO Rethink this
// https://gitlab.com/navenest/scavenger-hunts/-/raw/main/testing/hunt.json
//func TestWebIngest(t *testing.T) {
//	var err error
//	exampleFile, err := ioutil.ReadFile("../../examples/hunts/hunt.json")
//	if err != nil {
//		logz.Logger.Error("Unable to read json file",
//			zap.Error(err))
//	}
//	exampleData := Hunt{}
//	err = json.Unmarshal(exampleFile, &exampleData)
//	if err != nil {
//		logz.Logger.Error("Unable to unmarshal json file",
//			zap.Error(err))
//	}
//
//	currentPayload := exampleFile
//	if err != nil {
//		logz.Logger.Error("Unable to unmarshal json file",
//			zap.Error(err))
//	}
//
//	// Start a local HTTP server
//	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
//		// Test request parameters
//		//t.equals(t, req.URL.String(), "/some/path")
//		// Send response to be tested
//		rw.Header().Add("Content-Type", "application/json")
//		_, _ = rw.Write(currentPayload)
//		//_, _ = rw.Write([]byte(`OK`))
//	}))
//	// Close the server when test finishes
//	defer server.Close()
//
//	data := WebIngest(server.URL)
//
//	if !cmp.Equal(data, exampleData) {
//		t.Errorf("expected '%+v', got '%+v'", exampleData, data)
//		t.Error(cmp.Diff(exampleData, data))
//	}
//}
