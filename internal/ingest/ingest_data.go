package ingest

import (
	"database/sql"
	"encoding/json"
	"gitlab.com/navenest/scavenger-hunt/pkg/database/pg"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	"go.uber.org/zap"
	"io/ioutil"
)

type Hunt struct {
	Name       string      `json:"name"`
	Location   Location    `json:"location"`
	Challenges []Challenge `json:"challenges"`
	HuntId     int         `json:"hunt_id"`
}

type Location struct {
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type Hunts struct {
	Name  string `json:"name"`
	Hunts []Hunt `json:"hunts"`
}

type Challenge struct {
	Type     string   `json:"type"`
	Question string   `json:"question"`
	Answer   string   `json:"answer"`
	Location Location `json:"location"`
}

//type huntYaml struct {
//	Name       string      `yaml:"name"`
//	Location	locationYaml	`yaml:"location"`
//	Challenges []challengeYaml `yaml:"challenges"`
//}
//
//type locationYaml struct {
//	longitude float64 `yaml:"longitude"`
//	Latitude  float32 `yaml:"latitude"`
//}
//
//type huntsYaml struct {
//	Name  string `yaml:"name"`
//	Hunts []huntYaml `yaml:"hunts"`
//}
//
//type challengeYaml struct {
//	Type     string `yaml:"type"`
//	Question string `yaml:"question"`
//	Answer   string `yaml:"answer"`
//}

func FileIngest(path string) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		logz.Logger.Error("Unable to read json file",
			zap.Error(err))
	}
	data := Hunts{}
	err2 := json.Unmarshal(file, &data)
	if err2 != nil {
		logz.Logger.Error("Unable to unmarshal json file",
			zap.Error(err2))
	}
	logz.Logger.Info("File Contents",
		zap.String("name", data.Name))
	var huntId int
	var challengeId int
	var challengeIdQuestion int
	var challengeIdAnswer int
	for _, v := range data.Hunts {
		tx, err := pg.DB.Begin()
		stmt, err := tx.Prepare(`INSERT INTO hunts(HUNT_ID,TITLE,latitude,longitude) VALUES(nextval('hunt_id'),$1,$2,$3) ON CONFLICT (title) DO UPDATE SET latitude = $2,longitude = $3 RETURNING HUNT_ID`)

		if err != nil {
			logz.Logger.Error("Unable to prepare statement",
				zap.String("hunt", v.Name),
				zap.Error(err))
		}

		defer func(stmt *sql.Stmt) {
			err := stmt.Close()
			if err != nil {
				logz.Logger.Error("Unable to close statement",
					zap.Error(err))
			}
		}(stmt)

		err = stmt.QueryRow(v.Name, v.Location.Latitude, v.Location.Longitude).Scan(&huntId)
		if err != nil {
			logz.Logger.Error("Unable to perform query",
				zap.Error(err),
				zap.Int("id", huntId))
		}

		for _, challenge := range v.Challenges {
			//challengeStmt, err := tx.Prepare(`INSERT INTO challenge_info(CHALLENGE_ID,HUNT_ID) VALUES(nextval('challenge_id'),$1) RETURNING CHALLENGE_ID`)
			//
			//if err != nil {
			//	logz.Logger.Error("Unable to prepare statement",
			//		zap.Error(err))
			//}
			//
			//defer func(challengeStmt *sql.Stmt) {
			//	err := challengeStmt.Close()
			//	if err != nil {
			//		logz.Logger.Error("Unable to close statement",
			//			zap.Error(err))
			//	}
			//}(challengeStmt)
			//
			//err = challengeStmt.QueryRow(huntId).Scan(&challengeId)
			//if err != nil {
			//	logz.Logger.Error("Unable to perform query",
			//		zap.Error(err),
			//		zap.Int("id", challengeId))
			//}
			//
			////err = tx.Commit()
			//
			//if err != nil {
			//	logz.Logger.Error("Unable to commit query",
			//		zap.Error(err))
			//}

			challengeQuestionStmt, err := tx.Prepare(`INSERT INTO challenge_questions(CHALLENGE_QUESTION_ID,HUNT_ID,TYPE,QUESTION) VALUES(nextval('challenge_question_id'),$1,$2,$3) ON CONFLICT (hunt_id,type,question) DO UPDATE SET hunt_id = $1, type = $2, question = $3 RETURNING CHALLENGE_QUESTION_ID`)
			if err != nil {
				logz.Logger.Error("Unable to prepare statement",
					zap.Error(err))
			}

			defer func(challengeQuestionStmt *sql.Stmt) {
				err := challengeQuestionStmt.Close()
				if err != nil {
					logz.Logger.Error("Unable to close statement",
						zap.Error(err))
				}
			}(challengeQuestionStmt)

			err = challengeQuestionStmt.QueryRow(huntId, challenge.Type, challenge.Question).Scan(&challengeIdQuestion)
			if err != nil {
				logz.Logger.Error("Unable to perform query",
					zap.Error(err),
					zap.Int("id", challengeId))
			}

			//err = tx.Commit()

			challengeAnswerStmt, err := tx.Prepare(`INSERT INTO challenge_answers(challenge_answers_id,CHALLENGE_QUESTION_ID,HUNT_ID,latitude,longitude,answer) VALUES($1,$2,$3,$4,$5,$6) ON CONFLICT (hunt_id,challenge_question_id) DO UPDATE SET latitude = $4, longitude = $5, answer = $6 RETURNING challenge_answers_id`)
			if err != nil {
				logz.Logger.Error("Unable to prepare statement",
					zap.Error(err))
			}

			defer func(challengeAnswerStmt *sql.Stmt) {
				err := challengeAnswerStmt.Close()
				if err != nil {
					logz.Logger.Error("Unable to close statement",
						zap.Error(err))
				}
			}(challengeAnswerStmt)

			err = challengeAnswerStmt.QueryRow(challengeIdQuestion, challengeIdQuestion, huntId, challenge.Location.Latitude, challenge.Location.Longitude, challenge.Answer).Scan(&challengeIdAnswer)
			if err != nil {
				logz.Logger.Error("Unable to perform query",
					zap.Error(err),
					zap.Int("id", challengeId))
			}

			//err = tx.Commit()
		}
		err = tx.Commit()

		if err != nil {
			logz.Logger.Error("Unable to commit query",
				zap.Error(err))
		}

	}

}

// TODO Rethink this
//func WebIngest(URL string) Hunt {
//	resp, err := http.Get(URL)
//	if err != nil {
//		logz.Logger.Error("Unable to get content from URL for Web Data Ingest",
//			zap.Error(err),
//			zap.String("URL", URL))
//	}
//	defer func(Body io.ReadCloser) {
//		err := Body.Close()
//		if err != nil {
//			logz.Logger.Error("Unable to close body")
//		}
//	}(resp.Body)
//
//	if resp.StatusCode != http.StatusOK {
//		logz.Logger.Error("Unexpected Status Code",
//			zap.Int("Status Code", resp.StatusCode))
//		// Break out somehow?
//	}
//
//	data := Hunt{}
//	body, err := ioutil.ReadAll(resp.Body)
//	if err != nil {
//		logz.Logger.Error("Unable to Read body from web url",
//			zap.Error(err))
//	}
//	//logz.Logger.Debug("Web Url Content",
//	//	zap.ByteString("Body", body))
//	err = yaml.Unmarshal(body, &data)
//	if err != nil {
//		logz.Logger.Error("Unable to unmarshal body from web url",
//			zap.Error(err))
//	}
//
//	logz.Logger.Info("Web Contents",
//		zap.String("name", data.Name))
//
//	return data
//}
