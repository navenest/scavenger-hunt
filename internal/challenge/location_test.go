package challenge

import (
	"testing"
)

func TestDistance(t *testing.T) {
	type location struct {
		latitude  float64
		longitude float64
	}
	type test struct {
		title    string
		source   location
		dest     location
		distance float64
	}

	tests := []test{
		{
			"short distance",
			location{latitude: 28.37565, longitude: -81.54939},
			location{latitude: 28.37570, longitude: -81.54940},
			5.651461274786519,
		},
		{
			"same location",
			location{latitude: 28.37565, longitude: -81.54939},
			location{latitude: 28.37565, longitude: -81.54939},
			0,
		},
		{
			"long distance",
			location{latitude: 28.37565, longitude: -81.54939},
			location{latitude: 28.37565, longitude: 81.54939},
			13468086.068378257,
		},
	}

	for _, tc := range tests {
		checkDistance := func(t *testing.T, want error) {
			t.Helper()
			distance := Distance(tc.source.latitude, tc.source.longitude, tc.dest.latitude, tc.dest.longitude)
			if distance != tc.distance {
				t.Errorf("expected '%+v', got '%+v'", tc.distance, distance)
			}
		}
		t.Run(tc.title, func(t *testing.T) {
			checkDistance(t, nil)
		})
	}
}
