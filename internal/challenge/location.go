package challenge

import "math"

// Stolen from https://gist.github.com/cdipaolo/d3f8db3848278b49db68
func haversin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

func Distance(sourceLat, sourceLong, destLat, destLong float64) float64 {
	sourceLat = sourceLat * math.Pi / 180
	sourceLong = sourceLong * math.Pi / 180
	destLat = destLat * math.Pi / 180
	destLong = destLong * math.Pi / 180

	r := float64(6378100)
	h := haversin(destLat-sourceLat) + math.Cos(sourceLat)*math.Cos(destLat)*haversin(destLong-sourceLong)

	return 2 * r * math.Asin(math.Sqrt(h))
}
