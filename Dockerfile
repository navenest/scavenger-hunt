FROM node:latest as node-builder
WORKDIR /app
COPY /web/pwa/ .
RUN npm install
RUN npm run build --prod


######## Start a new stage from scratch #######


FROM golang:1.21 as golang-builder

ENV MIGRATIONS_DIR="/usr/local/go/src/scavenger-hunt/migrations"
WORKDIR /usr/local/go/src/scavenger-hunt

# Copy go mod and sum files
COPY go.mod go.sum ./
COPY . .
# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

#RUN go test -json ./...



# RUN CGO_ENABLED=0 go test ./... -v -cover
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main ./cmd/scavenger-hunt/

######## Start a new stage from scratch #######
FROM debian:bullseye-slim
ENV MIGRATIONS_DIR="/app/migrations"
WORKDIR /app/

RUN mkdir -p dist
RUN apt update && apt install -y ca-certificates

COPY examples/hunts/hunt.json .
COPY migrations .
COPY --from=node-builder /app/dist/pwa dist/
# Copy the Pre-built binary file from the previous stage
COPY --from=golang-builder /usr/local/go/src/scavenger-hunt/main .
#RUN chmod +x main


# Command to run the executable
CMD ["./main"]
