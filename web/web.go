package web

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"strings"

	//"github.com/prometheus/client_golang/prometheus"
	//"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/labstack/echo-contrib/prometheus"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	apiv1 "gitlab.com/navenest/scavenger-hunt/web/api/v1"
	"go.uber.org/zap"
	"net/http"
)

type App struct {
	Router    *echo.Echo
	ApiRouter *echo.Group
	//TODO Remove if Echo Pans out
	//Router    *mux.Router
	//ApiRouter *mux.Router
	apiv1 *apiv1.API
}

type health struct {
	Status string `json:"status"`
}

func (web *App) Initialize() {

	web.Router = echo.New()
	//TODO Remove if Echo Pans out
	//web.Router = mux.NewRouter()
	web.initializeRoutes()
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(web.Router)
	web.Router.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Skipper: func(c echo.Context) bool {
			if strings.HasPrefix(c.Request().RequestURI, "/healthcheck") || strings.HasPrefix(c.Request().RequestURI, "/metrics") {
				return true
			}
			return false
		},
	}))
	web.Router.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Skipper: func(c echo.Context) bool {
			return strings.Contains(c.Path(), "metrics") // Change "metrics" for your own path
		},
	}))
	//web.Router.HTTPErrorHandler = customHTTPErrorHandler
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	err := web.Router.Start(addr)
	if err != nil {
		logz.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}

//func customHTTPErrorHandler(err error, c echo.Context) {
//	httpError, ok := err.(*echo.HTTPError)
//	if ok {
//		errorCode := httpError.Code
//		switch errorCode {
//		case http.StatusNotFound:
//			return .Redirect(http.StatusMovedPermanently, "<URL>")
//		default:
//			// TODO handle any other case
//		}
//	}
//}
//
//e.HTTPErrorHandler = customHTTPErrorHandler

func (web *App) initializeRoutes() {
	//TODO Remove if Echo Pans out
	//web.ApiRouter = web.Router.PathPrefix("/api/v1/").Subrouter()
	//web.apiv1.InitializeRoutes(web.ApiRouter)
	//web.Router.Handle("/metrics", promhttp.Handler())
	//web.Router.HandleFunc("/healthcheck", web.healthcheck).Methods("GET")
	//web.Router.PathPrefix("/").Handler(http.FileServer(http.Dir("./dist/")))

	web.ApiRouter = web.Router.Group("/api/v1")
	web.apiv1.InitializeRoutes(web.ApiRouter)
	web.Router.GET("/healthcheck", web.healthcheck)
	//web.Router.Static("/", "./web/pwa/dist/pwa")
	//web.Router.File("/home", "./web/pwa/dist/pwa/index.html")
	web.Router.Static("/", "./dist")
	//web.Router.File("/*", "./dist/index.html")

	//web.Router.Use(middleware.StaticWithConfig(middleware.StaticConfig{
	//	Root:   "dist",
	//	Index:  "index.html",
	//	Browse: true,
	//	HTML5:  true,
	//}))
}

func (web *App) healthcheck(c echo.Context) error {
	data := health{
		Status: "UP",
	}
	return c.JSON(http.StatusOK, data)
}

//TODO Remove if Echo Pans out
//func (web *App) healthcheck(w http.ResponseWriter, r *http.Request) {
//	data := health{
//		Status: "UP",
//	}
//	w.Header().Set("Content-Type", "application/json")
//	w.WriteHeader(http.StatusOK)
//	err := json.NewEncoder(w).Encode(data)
//	if err != nil {
//		logz.Logger.Error("Unable to encode healthcheck data",
//			zap.Error(err))
//	}
//	//logz.Logger.Debug("Healthcheck Response",
//	//	zap.String("Location", "/healthcheck"))
//}
