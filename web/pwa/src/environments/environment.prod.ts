import {LogLevel} from "angular-auth-oidc-client";

export const environment = {
  production: true,
  apiUrl: "/api/v1/",
  authUrl: "https://keycloak.pineapplenest.com/realms/production",
  authLogs: LogLevel.Error,
};
