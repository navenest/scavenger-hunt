import { NgModule } from '@angular/core';
import {AuthInterceptor, AuthModule, LogLevel} from 'angular-auth-oidc-client';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import {environment} from "../../environments/environment";


@NgModule({
    imports: [AuthModule.forRoot({
        config: {
          //TODO Create as variable
              postLoginRoute: '/home',
              authority: environment.authUrl,
              redirectUrl: window.location.origin,
              postLogoutRedirectUri: window.location.origin,
              clientId: 'pkce',
              scope: 'openid profile email offline_access', // 'openid profile offline_access ' + your scopes
              responseType: 'code',
              silentRenew: true,
              useRefreshToken: true,
              autoUserInfo: true,
              renewTimeBeforeTokenExpiresInSeconds: 120,
              logLevel: environment.authLogs,
              secureRoutes: ['/api',],
              ignoreNonceAfterRefresh: true,
          }
      })],
    providers: [
      {
        provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
      }
    ],
    exports: [AuthModule],
})
export class AuthConfigModule {}
