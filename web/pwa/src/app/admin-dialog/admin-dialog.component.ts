import {Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MatInputModule} from '@angular/material/input';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Hunt, location} from "../shared/services/hunt-info";
import {firstValueFrom} from "rxjs";
import {environment} from "../../environments/environment";
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {MapService} from "../shared/services/map.service"

export interface DialogData {
  id: number;
  state: string;
}

export interface payload {
  name: string;
  location: location;
  challenges: [];
  hunt_id: number;
}

@Component({
  selector: 'app-admin-dialog',
  templateUrl: './admin-dialog.component.html',
  styleUrls: ['./admin-dialog.component.css']
})
export class AdminDialogComponent implements OnInit {

  huntList : Hunt[] = []
  name: string = ""
  payload!: Hunt

  constructor(
    public dialogRef: MatDialogRef<AdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public HuntInfoService: HuntInfoService,
    private http: HttpClient,
    private MapService: MapService,
  ) {}

  ngOnInit(): void {
    this.getMap()
  }

  getMap(): void {
    this.MapService.getMap()
    this.MapService.clickHandler()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async addData() {
    const headers = new HttpHeaders()
      .append(
        'Content-Type', 'application/json'
      )
    // let location: Array<number> | undefined = [0, 0]
    // location = this.MapService.getLocation()
    const currentLocation: location = {
      latitude: this.MapService.currentSelectedLocation[1],
      longitude: this.MapService.currentSelectedLocation[0]
    }
    if (this.data.state == "create") {
      this.payload = {
        name: this.name,
        location: currentLocation,
        challenges: [],
        hunt_id: 0,
      }
    }
    else {
      this.payload = {
        name: this.name,
        location: currentLocation,
        challenges: [],
        hunt_id: this.data.id,
      }
    }
    // const payload: Hunt = {
    //   name: this.name,
    //   location: currentLocation,
    //   challenges: [],
    //   hunt_id: 0,
    // }

    // console.log(payload)
    console.log(this.data)
    if (this.data.state == "create") {
      await firstValueFrom(this.http.post<any>(environment.apiUrl + 'hunt', this.payload, {
        headers: headers
      })).then(r => console.log(r))
    }
    else if (this.data.state == "edit") {
      await firstValueFrom(this.http.put<any>(environment.apiUrl + 'hunt/' + this.data.id, this.payload, {
        headers: headers
      })).then(r => console.log(r))
    }
    else if (this.data.state == "delete") {
      await firstValueFrom(this.http.delete<any>(environment.apiUrl + 'hunt/' + this.data.id, {
        headers: headers
      })).then(r => console.log(r))
    }

    let currentHunts = await this.HuntInfoService.getHuntsInfo()
    this.huntList = currentHunts.hunts
  }


}
