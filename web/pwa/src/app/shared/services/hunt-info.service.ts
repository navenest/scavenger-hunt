import {Injectable} from '@angular/core';
import {answer, challenge, Hunt, HuntAdmin, Hunts, location} from "./hunt-info";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, filter, firstValueFrom, lastValueFrom, retry, throwError} from "rxjs";
import { environment } from '../../../environments/environment';
import {OidcSecurityService} from "angular-auth-oidc-client";
import {MapService} from "./map.service"
import Geolocation from 'ol/Geolocation';



@Injectable({
  providedIn: 'root'
})
export class HuntInfoService {
  // public currentHunts: Hunts = {
  //   name: "",
  //   hunts: [],
  // }
  public currentHunts: Promise<Hunts> = this.getHuntInfo()
  public currentHuntIndex: number = 2
  public currentChallengeIndex: number = 1
  public currentHunt: Promise<Hunt> = this.getChallengeInfo(this.currentHuntIndex)
  public currentChallengeQuestion: string = ""
  private token: string = ""
  private static http: HttpClient;

  constructor(
    private http: HttpClient,
    private oidcSecurityService: OidcSecurityService,
    private MapService: MapService
  ) { }

  async getHuntInfo() {

    let hunt = await firstValueFrom(this.http.get<Hunts>(environment.apiUrl + 'hunts').pipe(retry(10), catchError(this.handleError)));
    this.currentHuntIndex = hunt.hunts[0].hunt_id
    console.log(this.currentHuntIndex)
    return hunt
    // let promise = new Promise(async (resolve, reject) => {
    //
    //
    // });
    //   (this.oidcSecurityService.getAccessToken().pipe(filter(result => !!result)).subscribe(async result => {
    //       console.log("PreValue", this.currentHunts)
    //       const huntData: Promise<Hunts> = firstValueFrom(this.http.get<Hunts>(environment.apiUrl + 'hunts').pipe(retry(10), catchError(this.handleError)));
    //       await huntData.then(response => {
    //         this.currentHunts = response;
    //         console.log(this.currentHunts)
    //       })
    //       console.log("PostValue", this.currentHunts)
    //   }));
    // this.oidcSecurityService.getAccessToken().subscribe(at => {
    //   console.log("Something", at)
    //
    // });
    // return firstValueFrom(this.http.get<Hunts>(environment.apiUrl + 'hunts').pipe(retry(10), catchError(this.handleError)));
  }
  //
  // getChallengeInfo(i:number) {
  //   return firstValueFrom(this.http.get<Hunt>(environment.apiUrl + 'hunts/'+ i).pipe(retry(10), catchError(this.handleError)));
  // }

  handleError(error:any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }

  async getChallengeInfo(i: number) {
    console.log("Getting challenge info")
    console.log(i)
    const isAuthorized = await lastValueFrom(this.oidcSecurityService.isAuthenticated())
    console.log(isAuthorized)
    let hunt
    let challengeList : challenge[] = []
    return await firstValueFrom(this.http.get<Hunt>(environment.apiUrl + 'hunt/'+ i));
    // challengeList = hunt.challenges
    // return challengeList
  }

  async getChallengeInfoAdmin(i: number) {
    console.log("Getting challenge info")
    console.log(i)
    return await firstValueFrom(this.http.get<HuntAdmin>(environment.apiUrl + 'huntadmin/'+ i));
    // challengeList = hunt.challenges
    // return challengeList
  }

  async getHuntsInfo() {
    console.log("Getting Hunt info")
    return await firstValueFrom(this.http.get<Hunts>(environment.apiUrl + 'hunts'));
    // challengeList = hunt.challenges
    // return challengeList
  }

  async submitAnswer() {
    const headers = new HttpHeaders()
      .append(
        'Content-Type', 'application/json'
      )
    let location: Array<number> | undefined = [0, 0]
    location = this.MapService.getLocation()
    const currentLocation: location = {
      latitude: location![1],
      longitude: location![0]
    }
    const payload: answer = {
      answer: "",
      location: currentLocation
    }

    console.log("sending answer")
    return await firstValueFrom(this.http.post<any>(environment.apiUrl + 'hunt/' + this.currentHuntIndex + '/' + this.currentChallengeIndex, payload, {
      headers: headers
    }))
  }



}


