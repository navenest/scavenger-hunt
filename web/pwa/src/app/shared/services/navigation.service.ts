import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public isMenuOpen: boolean = false;

  constructor() { }
}
