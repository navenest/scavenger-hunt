export interface location {
  longitude: number;
  latitude: number;
}

export interface Hunt {
  location: location;
  name: string;
  challenges: challenge[];
  hunt_id: number;
}

export interface challenge {
  question: string;
  type: string;
  challenge_question_id: number;
  correct: boolean;
}

export interface answer {
  answer: string;
  location: location;
}

export interface HuntAdmin {
  location: location;
  name: string;
  challenges: ChallengePayload[];
  hunt_id: number;
}

export interface ChallengePayload {
  question: string;
  type: string;
  answer: string;
  location: location;
  challenge_question_id: number;
}


export interface Hunts {
  name: string;
  hunts: Hunt[];
}

