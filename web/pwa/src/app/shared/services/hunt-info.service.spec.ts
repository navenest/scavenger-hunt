import { TestBed } from '@angular/core/testing';

import { HuntInfoService } from './hunt-info.service';

describe('HuntInfoService', () => {
  let service: HuntInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HuntInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
