import {Injectable} from '@angular/core';
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import View from "ol/View";
import {Hunts} from "./hunt-info";
import Style from "ol/style/Style";
import Icon from "ol/style/Icon";
import {Feature, getUid} from "ol";
import {Geometry, Point, Polygon} from "ol/geom";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {transform, useGeographic} from 'ol/proj';
import {Coordinate} from "ol/coordinate";
import {Router} from "@angular/router";
import Geolocation from 'ol/Geolocation';
import CircleStyle from "ol/style/Circle";
import {Fill, Stroke} from "ol/style";
import Draw from 'ol/interaction/Draw';
import {mark} from "@angular/compiler-cli/src/ngtsc/perf/src/clock";

@Injectable({
  providedIn: 'root'
})
export class MapService {
  map!: Map;
  huntInfo! : Hunts
  huntList : any
  long : number = 0
  public featureList : Feature[] = []
  public hovered: boolean = false
  public currentID: number = 0
  public currentSelectedLocation: Coordinate = []
  draw!: Draw;

  iconStyle: Style = new Style({
    image: new Icon({
      anchor: [0.5, 46],
      anchorXUnits: 'fraction',
      anchorYUnits: 'pixels',
      src: '../../assets/icons/fox-72x72.png',
    })
  })

  iconStyleSelected: Style = new Style({
    image: new Icon({
      anchor: [0.5, 46],
      anchorXUnits: 'fraction',
      anchorYUnits: 'pixels',
      src: '../../assets/icons/fox-72x72.png',
      opacity: 0.8
    })
  })

  positionFeature: Feature = new Feature()
  positionStyle: Style = new Style({
        image: new CircleStyle({
        radius: 6,
        fill: new Fill({
        color: '#3399CC',
      }),
      stroke: new Stroke({
        color: '#fff',
        width: 2,
        }),
      }),
    });

  geolocation: Geolocation = new Geolocation({
    // enableHighAccuracy must be set to true to have the heading value.
    trackingOptions: {
      enableHighAccuracy: true,
    },
  });

  coordinates: Array<number> | undefined = []







  constructor( public router:Router) { }

  getMap() {
    useGeographic();
    console.log("MAP")

    const epcot = [-81.54939, 28.37565]; //Temp for testing
    this.map = new Map({
      target: 'ol-map',
      layers: [
        new TileLayer({
          source: new OSM(),
          preload: 5,
        }),
      ],
      view: new View({
        center: this.geolocation.getPosition(),
        zoom: 15,
        projection: 'EPSG:4326'
      })
    });
    // const huntData = this.getHuntInfo();
    this.locationFeature()
    console.log("LOCATION")
  }



  flyTo(location: Coordinate) {
    const duration = 3000;
    const currentZoom = this.map.getView().getZoom()
    const zoomLevel = 20
    let parts = 2;
    let called = false;
    function callback() {
      --parts;
      if (called) {
        return;
      }
      if (parts === 0 ) {
        called = true;
      }
    }
    this.map.getView().animate({
        zoom: (currentZoom!) -3,
        duration: duration/2,
      },
      {
        zoom: zoomLevel,
        duration: duration/2,
      },
      callback
    )
    this.map.getView().animate( {
        center: location,
        duration: duration
      },
      callback
    )

  }

  hoverFeature() {
    const iconStyle = new Style({
      image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '../../assets/icons/fox-72x72.png',
        opacity: 0.8
      })
    })
    let selected: any = null;
    this.map.on('pointermove', (e) => {
      if (selected !== null) {
        if (selected.get("name") === "marker")
        selected.setStyle(undefined);
        selected = null;
        this.hovered = false
      }

      this.map.forEachFeatureAtPixel(e.pixel, (f) => {
        selected = f;
        if(selected.get("name") === "marker") {
          selected.setStyle(iconStyle)
          this.hovered = true
          this.currentID = selected.getId()
        }
      });
    });
  }

  highlightFeature(id: number) {
    this.featureList[id].setStyle(this.iconStyleSelected)
  }
  unhighlightFeature(id: number) {
    this.featureList[id].setStyle(this.iconStyle)
  }

  clickFeature() {
    let selected: any = null;
    this.map.on('click', (e) => {
      // if (selected !== null) {
      //   selected.setStyle(undefined);
      //   selected = null;
      // }

      this.map.forEachFeatureAtPixel(e.pixel, (f) => {
        selected = f;
        if(selected.get("name") === "marker")
        {
          // this.router.navigate(['hunt/'+(selected.getId())]).then()
          this.hovered = false
          return true;
        }
        return false
      });
    });
  }

  locationFeature() {
    this.geolocation.setTracking(true)
    this.geolocation.setProjection('EPSG:4326')
    const accuracyFeature = new Feature();
    this.geolocation.on('change:accuracyGeometry', () => {
      accuracyFeature.setGeometry(this.geolocation.getAccuracyGeometry()!);
    });

    this.geolocation.on('change:position', () => {
      const coordinates = this.geolocation.getPosition();
      console.log(coordinates)
      // const newCoordinates = transform(coordinates!,"EPSG:3857", "EPSG:4326")
      this.positionFeature.setGeometry(new Point(coordinates!))
      // this.positionFeature.setGeometry(new Polygon(new Point(this.geolocation.getPosition()!)));

    });
    this.positionFeature.setStyle(this.positionStyle)
    const vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [accuracyFeature, this.positionFeature]
      }),
    })
    this.map.addLayer(vectorLayer)
    vectorLayer.once('change', () =>
    {
      this.map.setView(new View({
        center: this.geolocation.getPosition(),
        zoom: 15
      }))
    })

  }

  getLocation() {
    this.geolocation.setTracking(true)
    this.geolocation.setProjection('EPSG:4326')
    return this.geolocation.getPosition();
  }

  clickHandler() {
    this.map.on('singleclick', (evt) => {
      const coordinate = evt.coordinate;
      this.currentSelectedLocation = coordinate
      console.log(coordinate)

      let marker = new Feature({
        type: 'icon',
        geometry: new Point(coordinate),
        name: 'marker'
      })
      marker.setId(0)

      const vectorLayer = new VectorLayer({
        source: new VectorSource({
          features: [marker]
        }),
        style: this.iconStyle
      })
      getUid(vectorLayer)
      this.map.getLayers().forEach(layer => {
        if (layer.get('name') && layer.get('name') == 'marker'){
          this.map.removeLayer(layer)
        }
      });
      vectorLayer.set('name', 'marker')
      this.map.addLayer(vectorLayer)
    })
    // this.map.addInteraction()
  }




}

