import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminChallengeDialogComponent } from './admin-challenge-dialog.component';

describe('AdminChallengeDialogComponent', () => {
  let component: AdminChallengeDialogComponent;
  let fixture: ComponentFixture<AdminChallengeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminChallengeDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminChallengeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
