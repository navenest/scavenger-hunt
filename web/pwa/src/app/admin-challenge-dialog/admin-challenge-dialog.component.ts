import {Component, Inject, OnInit} from '@angular/core';
import {Hunt, location, ChallengePayload} from "../shared/services/hunt-info";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {environment} from "../../environments/environment";
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {MapService} from "../shared/services/map.service";

export interface DialogData {
  id: number;
  challengeId: number;
  state: string;
}

@Component({
  selector: 'app-admin-challenge-dialog',
  templateUrl: './admin-challenge-dialog.component.html',
  styleUrls: ['./admin-challenge-dialog.component.css']
})

export class AdminChallengeDialogComponent implements OnInit {

  huntList : Hunt[] = []
  question: string = ""
  payload!: ChallengePayload


  constructor(
    public dialogRef: MatDialogRef<AdminChallengeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public HuntInfoService: HuntInfoService,
    private http: HttpClient,
    private MapService: MapService,
  ) {}

  ngOnInit(): void {
    this.getMap()
  }

  getMap(): void {
    this.MapService.getMap()
    this.MapService.clickHandler()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async addData() {
    const headers = new HttpHeaders()
      .append(
        'Content-Type', 'application/json'
      )
    // let location: Array<number> | undefined = [0, 0]
    // location = this.MapService.getLocation()
    const currentLocation: location = {
      latitude: this.MapService.currentSelectedLocation[1],
      longitude: this.MapService.currentSelectedLocation[0]
    }

    if (this.data.state == "create") {
      this.payload = {
        question: this.question,
        type: "location",
        answer: "",
        location: currentLocation,
        challenge_question_id: 0,
      }
    }
    else {
      this.payload = {
        question: this.question,
        type: "location",
        answer: "",
        location: currentLocation,
        challenge_question_id: this.data.challengeId,
      }
    }

    // const payload: ChallengePayload = {
    //   question: this.question,
    //   type: "location",
    //   answer: "",
    //   location: currentLocation,
    //   challenge_question_id: 0,
    // }

    console.log(this.data)
    if (this.data.state == "create") {
      await firstValueFrom(this.http.post<any>(environment.apiUrl + 'hunt/' + this.data.id + '/challenge', this.payload, {
        headers: headers
      })).then(r => console.log(r))
    }
    else if (this.data.state == "edit") {
      await firstValueFrom(this.http.put<any>(environment.apiUrl + 'hunt/' + this.data.id + '/' + this.data.challengeId, this.payload, {
        headers: headers
      })).then(r => console.log(r))
    }
    else if (this.data.state == "delete") {
      await firstValueFrom(this.http.delete<any>(environment.apiUrl + 'hunt/' + this.data.id + '/' + this.data.challengeId, {
        headers: headers
      })).then(r => console.log(r))
    }
    let currentHunts = await this.HuntInfoService.getHuntsInfo()
    this.huntList = currentHunts.hunts
  }

}
