import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {challenge, Hunt} from "../shared/services/hunt-info";
import {MapService} from "../shared/services/map.service";
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";


@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.css']
})
export class ChallengeComponent {

  constructor(public HuntInfoService: HuntInfoService, public bottomSheetRef: MatBottomSheetRef<ChallengeComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public currentHunt: Hunt,private MapService: MapService) { }

  ngAfterViewInit(): void {
    // this.getMap()
    console.log(this.currentHunt)
  }

  getMap(): void {
    this.MapService.getMap()
  }

  click(i: number) {
    this.HuntInfoService.currentChallengeIndex=this.currentHunt.challenges[i].challenge_question_id
    this.HuntInfoService.currentChallengeQuestion=this.currentHunt.challenges[i].question
    console.log(this.HuntInfoService.currentChallengeIndex)
    this.bottomSheetRef.dismiss()
  }

}
