import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MapService} from "../shared/services/map.service";
import Style from "ol/style/Style";
import Icon from "ol/style/Icon";
import {Feature} from "ol";
import {Point} from "ol/geom";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {HuntInfoService} from "../shared/services/hunt-info.service";


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit,AfterViewInit {


  constructor(private MapService: MapService, private HuntInfoService: HuntInfoService,) { }

  ngOnInit(): void {
    this.getMap()
    this.populateHomeMap()
    this.MapService.hoverFeature()
    this.MapService.clickFeature()

  }

  ngAfterViewInit(): void {

  }


  getMap(): void {
    this.MapService.getMap()
  }

  populateHomeMap() {
    const iconStyle = new Style({
      image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '../../assets/icons/fox-72x72.png',
        // opacity: 0.8
      })
    })
    this.HuntInfoService.getHuntInfo().then(response => {
      let i:number
      const len:number = response.hunts.length
      for (i=0;i<len;i++){
        let marker = new Feature({
          type: 'icon',
          geometry: new Point([response.hunts[i].location.longitude, response.hunts[i].location.latitude,]),
          name: 'marker'
        })
        marker.setId(i)
        this.MapService.featureList.push(marker)
      }
      const vectorLayer = new VectorLayer({
        source: new VectorSource({
          features: this.MapService.featureList
        }),
        style: iconStyle
      })
      this.MapService.map.addLayer(vectorLayer)
    })
  }




}
