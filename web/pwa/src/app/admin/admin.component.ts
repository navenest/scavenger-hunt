import { Component, OnInit, ViewChild } from '@angular/core';
import {Location} from '@angular/common';
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {MapService} from "../shared/services/map.service"

import {answer, challenge, Hunt, location} from "../shared/services/hunt-info";
import {MatTable} from '@angular/material/table';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {environment} from "../../environments/environment";
import {OidcSecurityService} from "angular-auth-oidc-client";
import {AdminDialogComponent} from "../admin-dialog/admin-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  huntList : Hunt[] = []

  displayedColumns: string[] = ['hunt_id', 'name', 'location', 'edit', 'delete'];

  constructor(
    public HuntInfoService: HuntInfoService,
    private http: HttpClient,
    private MapService: MapService,
    public dialog: MatDialog,
    public router:Router,
    private _location: Location
  ) { }

  async ngOnInit() {
    let currentHunts = await this.HuntInfoService.getHuntsInfo()
    this.huntList = currentHunts.hunts
    console.log(this.huntList)
  }
  @ViewChild(MatTable) table?: MatTable<Hunt>


  openDialog(): void {
    const dialogRef = this.dialog.open(AdminDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: 0, state: "create"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getHuntsInfo()
      this.huntList = currentHunts.hunts
      this.table?.renderRows();
    });
  }

  openDialogEdit(id: number, $event: { stopPropagation: () => void; }): void {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(AdminDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: id, state: "edit"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getHuntsInfo()
      this.huntList = currentHunts.hunts
      this.table?.renderRows();
    });
  }

  openDialogDelete(id: number, $event: { stopPropagation: () => void; }): void {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(AdminDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: id, state: "delete"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getHuntsInfo()
      this.huntList = currentHunts.hunts
      this.table?.renderRows();
    });
  }

  clickedRow(hunt: Hunt) {
    this.router.navigate(["/admin/"+hunt.hunt_id]).then()
  }

  backButton() {
    this._location.back();
  }
}
