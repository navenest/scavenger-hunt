import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import { AutoLoginAllRoutesGuard } from 'angular-auth-oidc-client';
import {AdminComponent} from "./admin/admin.component";
import {AdminChallengeComponent} from "./admin-challenge/admin-challenge.component";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  { path: "home", component: HomeComponent,  canActivate: [AutoLoginAllRoutesGuard] },
  { path: "admin", component: AdminComponent,  canActivate: [AutoLoginAllRoutesGuard] },
  { path: "admin/:id", component: AdminChallengeComponent,  canActivate: [AutoLoginAllRoutesGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
