import { Component, OnInit } from '@angular/core';
import {ChallengeComponent} from "../challenge/challenge.component";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {Router} from "@angular/router";
import {challenge, Hunt, Hunts} from "../shared/services/hunt-info";
import {NavigationService} from "../shared/services/navigation.service";
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {MatSnackBar,MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {ThemePalette} from "@angular/material/core";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  public currentHunt: Hunt = {name:"",hunt_id:0,location:{latitude:0,longitude:0},challenges:[{question:"",type:"",challenge_question_id:0,correct:false}]};
  public currentHunts!: Hunts
  public challenges: challenge[] = []
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  color: ThemePalette = 'accent';
  checked = false;
  disabled = false;


  constructor(
    public HuntInfoService: HuntInfoService,
    public NavigationService: NavigationService,
    private bottomSheet: MatBottomSheet,
    private ChallengeComponent: ChallengeComponent,
    private snackBar: MatSnackBar,
    public router:Router
  ) { }

  async ngOnInit() {

    this.currentHunts = await this.HuntInfoService.getHuntInfo()
    this.currentHunt = await this.HuntInfoService.getChallengeInfo(this.HuntInfoService.currentHuntIndex)
      // this.currentHunt = await this.HuntInfoService.getChallengeInfo(i)
      // this.challenges = currentHunt.challenges
      // console.log(this.currentHunt.challenges[0].question)
      // const i = this.HuntInfoService.currentHuntIndex
      // console.log(i)
      // this.currentHunt = await this.HuntInfoService.getChallengeInfo(i)
      // console.log(this.currentHunt.challenges[0])
  }

  ngAfterViewInit(): void {

  }

  async click() {
    // this.router.navigate(['']).then()
    this.NavigationService.isMenuOpen = false
    const i = this.HuntInfoService.currentHuntIndex
    console.log(i)
    // this.currentHunt = await this.HuntInfoService.currentHunt
    this.currentHunt = await this.HuntInfoService.getChallengeInfo(i)
    // this.HuntInfoService.currentHunts.then(hunts => this.challengeList = hunts.hunts[i].challenges)
    // console.log(this.currentHunt)
    const bottomSheet = this.bottomSheet.open(ChallengeComponent,
      {
        data: this.currentHunt,
      })

  }

  answerClick() {
    this.HuntInfoService.submitAnswer().then(r => {
      this.snackBar.open( r, 'kk', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 5000,
        panelClass: ['mat-toolbar', 'mat-primary', "snackbar"]
      });
    })

  }

  goToAdmin(): void {
    console.log("ADMIN")
    this.NavigationService.isMenuOpen = false
    this.router.navigate(["/admin"]).then()
  }

}
