import {Component, OnInit} from '@angular/core';
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {challenge, Hunt, location} from "../shared/services/hunt-info";
import {MapService} from "../shared/services/map.service";
import {NavigationService} from "../shared/services/navigation.service";
import {Router} from "@angular/router";
import {ChallengeComponent} from "../challenge/challenge.component";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {ScrollingModule} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-hunt-info',
  templateUrl: './hunt-info.component.html',
  styleUrls: ['./hunt-info.component.css']
})
export class HuntInfoComponent implements OnInit {
  huntList : Hunt[] = []
  opacity: number = 0.8
  background: string = 'darkgray'
  constructor(private HuntInfoService: HuntInfoService, public MapService: MapService, private NavigationService: NavigationService, public router:Router,
              private bottomSheet: MatBottomSheet, private ChallengeComponent: ChallengeComponent) { }

  currentHunt! : Hunt
  challengeList : challenge[] = []
  id : number = 0
  challenges: challenge[] = []

  async ngOnInit() {
    // this.HuntInfoService.getHuntInfo().then()
    let currentHunts = await this.HuntInfoService.getHuntInfo()
    this.huntList = currentHunts.hunts
    // await this.HuntInfoService.currentHunts.then(hunts => this.huntList = hunts.hunts)


  }
// TODO Update to support coming from hunt/:id to either flyto still or just directly go to location on map, or disable
  async click(i: number, location: location) {
    // this.router.navigate(['']).then()
    const coordinates = [location.longitude, location.latitude]
    this.MapService.flyTo(coordinates)
    this.NavigationService.isMenuOpen = false
    let currentHunts = await this.HuntInfoService.getHuntInfo()
    console.log(currentHunts.hunts)
    console.log(currentHunts.hunts[i])
    console.log(currentHunts.hunts[i].hunt_id)
    this.HuntInfoService.currentHuntIndex=currentHunts.hunts[i].hunt_id
    // console.log(i)
    // this.currentHunt = await this.HuntInfoService.currentHunt
    this.currentHunt = await this.HuntInfoService.getChallengeInfo(this.HuntInfoService.currentHuntIndex)
    // this.currentHunt = await this.HuntInfoService.getChallengeInfo(i)
    // this.HuntInfoService.currentHunts.then(hunts => this.challengeList = hunts.hunts[i].challenges)
    // console.log(this.currentHunt)
    const bottomSheet = this.bottomSheet.open(ChallengeComponent,
      {
        data: this.currentHunt,
      })

  }



  //In use
  highlightMap(id: number) {
    this.MapService.highlightFeature(id)
  }
  //In use
  unhighlightMap(id: number) {
    this.MapService.unhighlightFeature(id)
  }

}
