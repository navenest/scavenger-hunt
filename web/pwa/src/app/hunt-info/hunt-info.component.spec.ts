import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HuntInfoComponent } from './hunt-info.component';

describe('HuntInfoComponent', () => {
  let component: HuntInfoComponent;
  let fixture: ComponentFixture<HuntInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HuntInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HuntInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
