import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {HuntInfoService} from "../shared/services/hunt-info.service";
import {Hunt, HuntAdmin} from "../shared/services/hunt-info";
import {MatTable} from "@angular/material/table";
import {AdminDialogComponent} from "../admin-dialog/admin-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {AdminChallengeDialogComponent} from "../admin-challenge-dialog/admin-challenge-dialog.component";
import {Location} from "@angular/common";

@Component({
  selector: 'app-admin-challenge',
  templateUrl: './admin-challenge.component.html',
  styleUrls: ['./admin-challenge.component.css']
})
export class AdminChallengeComponent implements OnInit {

  displayedColumns: string[] = ['challenge_question_id', 'question', 'location', 'edit', 'delete'];
  id: number = 0
  currentHunt: HuntAdmin = {
    name:"",
    hunt_id:0,
    location:{
      latitude:0,
      longitude:0
    },
    challenges:[
      {
        question:"",
        type:"",
        challenge_question_id:0,
        answer: "",
        location:{
          latitude:0,
          longitude:0
        }
      }
      ]
  };

  constructor(
    private route: ActivatedRoute,
    public HuntInfoService: HuntInfoService,
    public dialog: MatDialog,
    private _location: Location
  ) { }

  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']
    })
    console.log(this.currentHunt)
    this.currentHunt = await this.HuntInfoService.getChallengeInfoAdmin(this.id)
    console.log(this.currentHunt)
  }
  @ViewChild(MatTable) table?: MatTable<HuntAdmin>


  openDialog(): void {
    const dialogRef = this.dialog.open(AdminChallengeDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: this.id, state: "create"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getChallengeInfoAdmin(this.id)
      this.currentHunt = currentHunts
      this.table?.renderRows();
    });
  }

  openDialogEdit(challengeId: number, $event: { stopPropagation: () => void; }): void {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(AdminChallengeDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: this.id, challengeId: challengeId, state: "edit"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getChallengeInfoAdmin(this.id)
      this.currentHunt = currentHunts
      this.table?.renderRows();
    });
  }

  openDialogDelete(challengeId: number, $event: { stopPropagation: () => void; }): void {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(AdminChallengeDialogComponent, {
      width: "95vh",
      height: "95vh",
      data: {id: this.id, challengeId: challengeId, state: "delete"}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      let currentHunts = await this.HuntInfoService.getChallengeInfoAdmin(this.id)
      this.currentHunt = currentHunts
      this.table?.renderRows();
    });
  }

  backButton() {
    this._location.back();
  }

  // clickedRow(hunt: Hunt) {
  //   this.router.navigate(["/admin/"+hunt.hunt_id]).then()
  // }
}
