package v1

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/xenitab/go-oidc-middleware/oidcechojwt"
	"github.com/xenitab/go-oidc-middleware/options"
	"gitlab.com/navenest/scavenger-hunt/internal/challenge"
	"gitlab.com/navenest/scavenger-hunt/internal/ingest"
	"gitlab.com/navenest/scavenger-hunt/pkg/database/pg"
	logz "gitlab.com/navenest/scavenger-hunt/pkg/logger/log"
	"go.uber.org/zap"
	"golang.org/x/oauth2"
	"net/http"
	"os"
	"strconv"
)

type Hunt struct {
	Name       string          `json:"name"`
	Location   Location        `json:"location"`
	Challenges []ChallengeBody `json:"challenges"`
	HuntId     int             `json:"hunt_id"`
}

type HuntAdmin struct {
	Name       string             `json:"name"`
	Location   Location           `json:"location"`
	Challenges []ChallengePayload `json:"challenges"`
	HuntId     int                `json:"hunt_id"`
}

type Location struct {
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type Hunts struct {
	Name  string `json:"name"`
	Hunts []Hunt `json:"hunts"`
}

type Challenge struct {
	Type                string `json:"type"`
	Question            string `json:"question"`
	ChallengeQuestionId int    `json:"challenge_question_id"`
}

type ChallengePayload struct {
	Type                string   `json:"type"`
	Question            string   `json:"question"`
	Answer              string   `json:"answer"`
	Location            Location `json:"location"`
	ChallengeQuestionId int      `json:"challenge_question_id"`
}

type ChallengeBody struct {
	Type                string   `json:"type"`
	Question            string   `json:"question"`
	Correct             bool     `json:"correct"`
	Location            Location `json:"location"`
	ChallengeQuestionId int      `json:"challenge_question_id"`
}

type health struct {
	Status string `json:"status"`
}

type API struct {
	Status string `json:"status"`
	Router *echo.Group
	//Router *mux.Router
	Hunts
}

type Import struct {
	Source string `json:"source"`
	Path   string `json:"path"`
	URL    string `json:"URL"`
}

type Answer struct {
	Answer   string   `json:"answer"`
	Location Location `json:"location"`
}

type Middleware func(http.HandlerFunc) http.HandlerFunc

//TODO Make this better, move this outside of just the global vars?

var (
	clientID     = os.Getenv("OAUTH2_CLIENT_ID")
	clientSecret = os.Getenv("OAUTH2_CLIENT_SECRET")
	idpURL       = os.Getenv("IDP_URL")
	callbackURL  = os.Getenv("CALLBACK_URL")
	baseURL      = os.Getenv("BASE_URL")
	ctx          = context.Background()
	config       oauth2.Config
	verifier     *oidc.IDTokenVerifier
	provider     *oidc.Provider
)

var CurrentHunts Hunts

func (api *API) HuntImport(c echo.Context) error {
	data := new(Import)
	err := c.Bind(data)
	if err != nil {
		logz.Logger.Error("Unable to bind json body",
			zap.Error(err))
		return c.String(500, "Unable to bind requested body")
	}
	if data.Source == "file" {
		ingest.FileIngest(data.Path)
		logz.Logger.Info("Hunt has been added",
			zap.String("Current Hunt Package Name", CurrentHunts.Name))

		return c.String(202, "Hunt Imported")
	}
	return c.String(404, "Failed to import Hunt")
}

func (api *API) CreateHunt(c echo.Context) error {

	data := new(Hunt)
	err := c.Bind(data)
	if err != nil {
		logz.Logger.Error("Unable to bind json body",
			zap.Error(err))
		return err
	}

	logz.Logger.Info("Creating Hunt",
		zap.String("hunt", data.Name),
		zap.Any("payload", data))

	tx, err := pg.DB.Begin()
	stmt, err := tx.Prepare(`INSERT INTO hunts(HUNT_ID,TITLE,latitude,longitude) VALUES(nextval('hunt_id'),$1,$2,$3) ON CONFLICT (title) DO UPDATE SET latitude = $2,longitude = $3 RETURNING HUNT_ID`)

	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.String("hunt", data.Name),
			zap.Error(err))
	}

	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
			logz.Logger.Error("Unable to close statement",
				zap.Error(err))
		}
	}(stmt)
	var huntId int
	err = stmt.QueryRow(data.Name, data.Location.Latitude, data.Location.Longitude).Scan(&huntId)
	if err != nil {
		logz.Logger.Error("Unable to perform query",
			zap.Error(err),
			zap.Int("id", huntId))
	}
	data.HuntId = huntId

	err = tx.Commit()

	if err != nil {
		logz.Logger.Error("Unable to commit query",
			zap.Error(err))
	}
	return c.JSON(201, data)
}

func (api *API) CreateChallenge(c echo.Context) error {
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("huntId", c.Param("huntId")))
	huntId, err := strconv.Atoi(c.Param("huntId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}

	data := new(ChallengePayload)
	err = c.Bind(data)
	if err != nil {
		logz.Logger.Error("Unable to bind json body",
			zap.Error(err))
		return err
	}
	tx, err := pg.DB.Begin()
	stmt, err := tx.Prepare(`INSERT INTO challenge_questions(CHALLENGE_QUESTION_ID,HUNT_ID,TYPE,QUESTION) VALUES(nextval('challenge_question_id'),$1,$2,$3) ON CONFLICT (hunt_id,type,question) DO UPDATE SET hunt_id = $1, type = $2, question = $3 RETURNING CHALLENGE_QUESTION_ID`)

	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.Int("huntId", huntId),
			zap.Error(err))
		return err
	}

	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
			logz.Logger.Error("Unable to close statement",
				zap.Error(err))
		}
	}(stmt)
	var challengeId int
	err = stmt.QueryRow(huntId, data.Type, data.Question).Scan(&challengeId)
	if err != nil {
		logz.Logger.Error("Unable to perform query",
			zap.Error(err),
			zap.Int("id", huntId))
		return err
	}
	data.ChallengeQuestionId = challengeId

	//err = tx.Commit()
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to commit query",
	//		zap.Error(err))
	//	return err
	//}
	answersStmt, err := tx.Prepare(`INSERT INTO challenge_answers(challenge_answers_id,CHALLENGE_QUESTION_ID,HUNT_ID,latitude,longitude,answer) VALUES($1,$2,$3,$4,$5,$6) ON CONFLICT (hunt_id,challenge_question_id) DO UPDATE SET latitude = $4, longitude = $5, answer = $6 RETURNING challenge_answers_id`)

	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}

	defer func(answersStmt *sql.Stmt) {
		err := answersStmt.Close()
		if err != nil {
			logz.Logger.Error("Unable to close statement",
				zap.Error(err))
		}
	}(answersStmt)
	answersStmt.QueryRow(challengeId, challengeId, huntId, data.Location.Latitude, data.Location.Longitude, data.Answer)
	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	data.ChallengeQuestionId = challengeId

	err = tx.Commit()

	if err != nil {
		logz.Logger.Error("Unable to commit query",
			zap.Error(err))
		return err
	}
	return c.JSON(201, data)
}

func (api *API) UpdateHunt(c echo.Context) error {
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("ID", c.Param("id")))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Hunt via ID",
		zap.Int("ID", id))
	data := new(Hunt)
	err = c.Bind(data)
	if err != nil {
		logz.Logger.Error("Unable to bind json body",
			zap.Error(err))
		return err
	}
	//tx, err := pg.DB.Begin()
	//stmt, err := tx.Prepare(`UPDATE hunts SET title=$1, latitude=$2, longitude=$3 WHERE hunt_id = $4`)
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to prepare statement",
	//		zap.String("hunt", data.Name),
	//		zap.Error(err))
	//}
	//
	//defer func(stmt *sql.Stmt) {
	//	err := stmt.Close()
	//	if err != nil {
	//		logz.Logger.Error("Unable to close statement",
	//			zap.Error(err))
	//	}
	//}(stmt)
	//var huntId int
	//stmt.QueryRow(data.Name, data.Location.Latitude, data.Location.Longitude, data.HuntId)
	//if err != nil {
	//	logz.Logger.Error("Unable to perform query",
	//		zap.Error(err),
	//		zap.Int("id", huntId))
	//}
	//data.HuntId = huntId
	//
	//err = tx.Commit()
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to commit query",
	//		zap.Error(err))
	//}

	query := `UPDATE hunts SET title=$1, latitude=$2, longitude=$3 WHERE hunt_id = $4`
	res, err := pg.DB.Exec(query, data.Name, data.Location.Latitude, data.Location.Longitude, data.HuntId)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))
	return c.JSON(201, data)
}

func (api *API) DeleteHunt(c echo.Context) error {
	logz.Logger.Info("Deleting Hunt via ID",
		zap.String("ID", c.Param("id")))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Hunt via ID",
		zap.Int("ID", id))
	answerQuery := `DELETE FROM challenge_answers WHERE hunt_id = $1`

	res, err := pg.DB.Exec(answerQuery, id)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))

	questionQuery := `DELETE FROM challenge_questions WHERE hunt_id = $1`
	res, err = pg.DB.Exec(questionQuery, id)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	count, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))
	query := `DELETE FROM hunts WHERE hunt_id = $1`
	res, err = pg.DB.Exec(query, id)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	count, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", id),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))
	return c.JSON(201, id)
}

func (api *API) UpdateChallenge(c echo.Context) error {
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("huntId", c.Param("huntId")))
	huntId, err := strconv.Atoi(c.Param("huntId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Challenge via ID",
		zap.String("challengeId", c.Param("challengeId")))
	challengeId, err := strconv.Atoi(c.Param("challengeId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}

	data := new(ChallengePayload)
	err = c.Bind(data)
	if err != nil {
		logz.Logger.Error("Unable to bind json body",
			zap.Error(err))
		return err
	}
	//tx, err := pg.DB.Begin()
	//stmt, err := tx.Prepare(`UPDATE challenge_questions SET question=$1, type=$2 WHERE hunt_id = $3 and challenge_question_id = $4`)
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to prepare statement",
	//		zap.Int("huntId", huntId),
	//		zap.Int("challengeId", challengeId),
	//		zap.Error(err))
	//	return err
	//}
	//
	//defer func(stmt *sql.Stmt) {
	//	err := stmt.Close()
	//	if err != nil {
	//		logz.Logger.Error("Unable to close statement",
	//			zap.Error(err))
	//	}
	//}(stmt)
	//stmt.QueryRow(data.Type, data.Question, huntId, challengeId)
	//if err != nil {
	//	logz.Logger.Error("Unable to perform query",
	//		zap.Error(err),
	//		zap.Int("id", huntId))
	//	return err
	//}
	//data.ChallengeQuestionId = challengeId
	//
	//err = tx.Commit()
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to commit query",
	//		zap.Error(err))
	//	return err
	//}
	//stmt, err = tx.Prepare(`UPDATE challenge_answers SET latitude=$1, longitude=$2, answer=$3 WHERE hunt_id = $4 and challenge_question_id = $5 and challenge_answers_id = $5`)
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to prepare statement",
	//		zap.Int("huntId", huntId),
	//		zap.Int("challengeId", challengeId),
	//		zap.Error(err))
	//	return err
	//}
	//
	//defer func(stmt *sql.Stmt) {
	//	err := stmt.Close()
	//	if err != nil {
	//		logz.Logger.Error("Unable to close statement",
	//			zap.Error(err))
	//	}
	//}(stmt)
	//stmt.QueryRow(data.Location.Latitude, data.Location.Longitude, data.Answer, huntId, challengeId)
	//if err != nil {
	//	logz.Logger.Error("Unable to prepare statement",
	//		zap.Int("huntId", huntId),
	//		zap.Int("challengeId", challengeId),
	//		zap.Error(err))
	//	return err
	//}
	//data.ChallengeQuestionId = challengeId
	//
	//err = tx.Commit()
	//
	//if err != nil {
	//	logz.Logger.Error("Unable to commit query",
	//		zap.Error(err))
	//	return err
	//}
	answerQuery := `UPDATE challenge_answers SET latitude=$1, longitude=$2, answer=$3 WHERE hunt_id = $4 and challenge_question_id = $5 and challenge_answers_id = $5`

	res, err := pg.DB.Exec(answerQuery, data.Location.Latitude, data.Location.Longitude, data.Answer, huntId, challengeId)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))

	questionQuery := `UPDATE challenge_questions SET question=$1, type=$2 WHERE hunt_id = $3 and challenge_question_id = $4`
	res, err = pg.DB.Exec(questionQuery, data.Question, data.Type, huntId, challengeId)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	count, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))

	return c.JSON(201, data)
}

func (api *API) DeleteChallenge(c echo.Context) error {
	logz.Logger.Info("Deleting Challenge from ID",
		zap.String("huntId", c.Param("huntId")))
	huntId, err := strconv.Atoi(c.Param("huntId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Deleting Challenge via ID",
		zap.String("challengeId", c.Param("challengeId")))
	challengeId, err := strconv.Atoi(c.Param("challengeId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}

	answerQuery := `DELETE FROM challenge_answers WHERE hunt_id = $1 and challenge_question_id = $2 and challenge_answers_id = $2`

	res, err := pg.DB.Exec(answerQuery, huntId, challengeId)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))

	questionQuery := `DELETE FROM challenge_questions WHERE hunt_id = $1 and challenge_question_id = $2`
	res, err = pg.DB.Exec(questionQuery, huntId, challengeId)
	if err != nil {
		logz.Logger.Error("Error Executing Delete",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	count, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Error to count rows affected",
			zap.Int("huntId", huntId),
			zap.Int("challengeId", challengeId),
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Rows Deleted from challenge answers",
		zap.Int64("count", count))
	return c.JSON(201, challengeId)
}

func (api *API) GetHunt(c echo.Context) error {
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("ID", c.Param("id")))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Hunt via ID",
		zap.Int("ID", id))

	user := c.Get("user")
	userMap := user.(map[string]interface{})
	currentUser := userMap["sub"].(string)

	hunt := Hunt{}
	huntRows, err := pg.DB.Query(`SELECT title,latitude,longitude FROM hunts WHERE hunt_id=$1`, id)
	if err != nil {
		logz.Logger.Error("Unable to query for challenge questions",
			zap.Error(err),
			zap.Int("id", id))
	}
	defer func(huntRows *sql.Rows) {
		err := huntRows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close huntRows connection",
				zap.Error(err))
		}
	}(huntRows)
	for huntRows.Next() {
		var title string
		var latitude float64
		var longitude float64
		err := huntRows.Scan(&title, &latitude, &longitude)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}
		hunt = Hunt{
			Name: title,
			Location: Location{
				Latitude:  latitude,
				Longitude: longitude,
			},
		}
	}

	rows, err := pg.DB.Query(`SELECT question,type,challenge_question_id FROM challenge_questions WHERE hunt_id=$1`, id)
	if err != nil {
		logz.Logger.Error("Unable to query for challenge questions",
			zap.Error(err),
			zap.Int("id", id))
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rows)

	for rows.Next() {
		var question string
		var challengeType string
		var challengeQuestionId int
		err := rows.Scan(&question, &challengeType, &challengeQuestionId)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}

		currentChallenge := ChallengeBody{
			Question:            question,
			Type:                challengeType,
			ChallengeQuestionId: challengeQuestionId,
			Correct:             false,
		}

		hunt.Challenges = append(hunt.Challenges, currentChallenge)
	}

	rowsUser, err := pg.DB.Query(`SELECT challenge_question_id,correct FROM user_answers WHERE hunt_id=$1 and user_id=$2`, id, currentUser)
	if err != nil {
		logz.Logger.Error("Unable to query for user answers",
			zap.Error(err),
			zap.Int("id", id))
	}
	defer func(rowsUser *sql.Rows) {
		err := rowsUser.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rowsUser)

	for rowsUser.Next() {
		var challengeQuestionId int
		var correct bool
		err := rowsUser.Scan(&challengeQuestionId, &correct)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}

		for i, v := range hunt.Challenges {
			if v.ChallengeQuestionId == challengeQuestionId {
				hunt.Challenges[i].Correct = correct
				logz.Logger.Info("Updating correct statement",
					zap.Int("index", i),
					zap.Bool("correct", correct),
					zap.Int("challengeID", v.ChallengeQuestionId))
			}
		}
	}

	logz.Logger.Info("payload response",
		zap.Any("body", hunt))
	return c.JSON(200, hunt)
}

func (api *API) GetHuntAdmin(c echo.Context) error {
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("ID", c.Param("id")))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Hunt via ID",
		zap.Int("ID", id))
	hunt := HuntAdmin{}
	huntRows, err := pg.DB.Query(`SELECT title,latitude,longitude FROM hunts WHERE hunt_id=$1`, id)
	if err != nil {
		logz.Logger.Error("Unable to query for challenge questions",
			zap.Error(err),
			zap.Int("id", id))
	}
	defer func(huntRows *sql.Rows) {
		err := huntRows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close huntRows connection",
				zap.Error(err))
		}
	}(huntRows)
	for huntRows.Next() {
		var title string
		var latitude float64
		var longitude float64
		err := huntRows.Scan(&title, &latitude, &longitude)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}
		hunt = HuntAdmin{
			Name: title,
			Location: Location{
				Latitude:  latitude,
				Longitude: longitude,
			},
		}
	}

	rows, err := pg.DB.Query(`SELECT question,type,challenge_question_id FROM challenge_questions WHERE hunt_id=$1`, id)
	if err != nil {
		logz.Logger.Error("Unable to query for challenge questions",
			zap.Error(err),
			zap.Int("id", id))
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rows)

	for rows.Next() {
		var question string
		var challengeType string
		var challengeQuestionId int
		err := rows.Scan(&question, &challengeType, &challengeQuestionId)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}

		answerRows, err := pg.DB.Query(`SELECT latitude,longitude,answer FROM challenge_answers WHERE challenge_question_id=$1`, challengeQuestionId)
		if err != nil {
			logz.Logger.Error("Unable to query for challenge questions",
				zap.Error(err),
				zap.Int("id", id))
		}
		//defer func(answerRows *sql.Rows) {
		//	err := answerRows.Close()
		//	if err != nil {
		//		logz.Logger.Error("Unable to close rows connection",
		//			zap.Error(err))
		//	}
		//}(answerRows)
		answerRows.Next()
		var answer string
		var latitude float64
		var longitude float64
		err = answerRows.Scan(&latitude, &longitude, &answer)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}

		currentChallenge := ChallengePayload{
			Question:            question,
			Type:                challengeType,
			Answer:              answer,
			Location:            Location{Latitude: latitude, Longitude: longitude},
			ChallengeQuestionId: challengeQuestionId,
		}

		err = answerRows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}

		hunt.Challenges = append(hunt.Challenges, currentChallenge)
	}
	return c.JSON(200, hunt)
}

func (api *API) GetHunts(c echo.Context) error {
	rows, err := pg.DB.Query(`SELECT hunt_id,title,latitude,longitude FROM hunts`)
	if err != nil {
		logz.Logger.Error("Unable to query for hunts",
			zap.Error(err))
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rows)
	hunt := Hunts{}
	for rows.Next() {
		var id int
		var title string
		var latitude float64
		var longitude float64
		err := rows.Scan(&id, &title, &latitude, &longitude)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}
		logz.Logger.Info("title",
			zap.String("title", title))

		currentHunt := Hunt{
			Name: title,
			Location: Location{
				Latitude:  latitude,
				Longitude: longitude,
			},
			HuntId: id,
		}

		hunt.Hunts = append(hunt.Hunts, currentHunt)
	}

	//hunt := CurrentHunts

	//err := json.NewEncoder(w).Encode(hunt)
	//if err != nil {
	//	logz.Logger.Error("Unable to encode current hunt data",
	//		zap.Error(err))
	//}
	logz.Logger.Info("Hunt retrieved",
		zap.String("Current Hunt Package Name", hunt.Name))
	//w.WriteHeader(200)
	return c.JSON(200, hunt)
}

func (api *API) SubmitAnswer(c echo.Context) error {
	logz.Logger.Info("Started submit Answer")
	// Get User info from oauth data
	data := new(Answer)
	if err := c.Bind(data); err != nil {
		logz.Logger.Error("Unable to bind data",
			zap.Error(err))
		return c.String(500, "Invalid Payload")
	}
	user := c.Get("user")
	userMap := user.(map[string]interface{})
	currentUser := userMap["sub"].(string)
	logz.Logger.Info("user",
		zap.String("user", currentUser))

	// Get Id for hunt, and challenge
	logz.Logger.Info("Getting Hunt via ID",
		zap.String("huntId", c.Param("huntId")))
	huntId, err := strconv.Atoi(c.Param("huntId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}
	logz.Logger.Info("Getting Challenge via ID",
		zap.String("challengeId", c.Param("challengeId")))
	challengeId, err := strconv.Atoi(c.Param("challengeId"))
	if err != nil {
		logz.Logger.Error("Unable convert id to int",
			zap.Error(err))
		return c.String(415, "Unable to convert ID to int")
	}

	rows, err := pg.DB.Query(`SELECT latitude,longitude,answer FROM challenge_answers WHERE (hunt_id=$1 AND challenge_question_id=$2)`, huntId, challengeId)
	if err != nil {
		logz.Logger.Error("Unable to query for hunts",
			zap.Error(err))
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rows)
	currentAnswer := Answer{}
	for rows.Next() {
		var answer string
		var latitude float64
		var longitude float64
		err := rows.Scan(&latitude, &longitude, &answer)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}

		currentAnswer = Answer{
			Answer: answer,
			Location: Location{
				Latitude:  latitude,
				Longitude: longitude,
			},
		}
	}
	//err = rows.Close()
	//if err != nil {
	//	logz.Logger.Error("Unable to close rows connection",
	//		zap.Error(err))
	//}
	//logz.Logger.Info("CurrentAnswer",
	//	zap.Any("answer", currentAnswer),
	//	zap.Any("submitted", data))
	distance := challenge.Distance(currentAnswer.Location.Latitude, currentAnswer.Location.Longitude, data.Location.Latitude, data.Location.Longitude)
	logz.Logger.Info("Distance",
		zap.Float64("distance", distance))
	logz.Logger.Info("Ending submit Answer")
	correct := false
	if distance < 25 {
		correct = true
	}

	tx, err := pg.DB.Begin()
	stmt, err := tx.Prepare(`INSERT INTO user_answers(user_id,challenge_answers_id,CHALLENGE_QUESTION_ID,HUNT_ID,latitude,longitude,answer, correct) VALUES($1,$2,$3,$4,$5,$6,$7,$8) ON CONFLICT (user_id,hunt_id,challenge_question_id) DO UPDATE SET latitude = $5, longitude = $6, answer = $7, correct = $8`)
	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.Error(err))
		return err
	}

	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
			logz.Logger.Error("Unable to close statement",
				zap.Error(err))
		}
	}(stmt)

	stmt.QueryRow(currentUser, challengeId, challengeId, huntId, data.Location.Latitude, data.Location.Longitude, data.Answer, correct)

	//if err != nil {
	//	logz.Logger.Error("Unable to perform insert",
	//		zap.Error(err),
	//		zap.Int("id", challengeId))
	//	return err
	//}
	err = tx.Commit()
	//err = stmt.Close()
	if err != nil {
		logz.Logger.Error("Unable to commit query",
			zap.Error(err))
		return err
	}
	logz.Logger.Info("Submitted Insert")

	if correct {
		return c.JSON(200, "correct")
	} else {
		return c.JSON(200, "incorrect")
	}

}

func (api *API) InitializeRoutes(apiRouter *echo.Group) {
	apiRouter.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		ParseTokenFunc: oidcechojwt.New(
			options.WithIssuer(idpURL),
			options.WithRequiredTokenType("JWT"),
			options.WithRequiredAudience(clientID),
			options.WithFallbackSignatureAlgorithm("RS256"),
			//options.WithRequiredClaims(map[string]interface{}{
			//	"tid": cfg.TenantID,
			//}),
		),
		SuccessHandler: successHandler,
	}))
	apiRouter.GET("/healthcheck", api.healthcheck)
	apiRouter.POST("/import", api.HuntImport)
	apiRouter.POST("/hunt", api.CreateHunt)
	apiRouter.GET("/hunt/:id", api.GetHunt)
	apiRouter.GET("/huntadmin/:id", api.GetHuntAdmin)
	apiRouter.PUT("/hunt/:id", api.UpdateHunt)
	apiRouter.DELETE("/hunt/:id", api.DeleteHunt)
	apiRouter.GET("/hunts", api.GetHunts)
	apiRouter.POST("/hunt/:huntId/:challengeId", api.SubmitAnswer)
	apiRouter.POST("/hunt/:huntId/challenge", api.CreateChallenge)
	apiRouter.PUT("/hunt/:huntId/:challengeId", api.UpdateChallenge)
	apiRouter.DELETE("/hunt/:huntId/:challengeId", api.DeleteChallenge)
}

func successHandler(c echo.Context) {
	user := c.Get("user")
	userMap := user.(map[string]interface{})
	query := fmt.Sprintf(`SELECT user_id FROM users WHERE user_id='%s';`, userMap["sub"].(string))
	data, err := pg.DB.Query(query)
	if err != nil {
		logz.Logger.Error("Unable to run Query",
			zap.Error(err),
			zap.String("query", query))
	}
	//logz.Logger.Info("Length of results",
	//	zap.Int("length", data.))
	if data.Next() == false {
		insert := fmt.Sprintf(`INSERT INTO users (user_id) VALUES ('%s');`, userMap["sub"].(string))
		res, err := pg.DB.Exec(insert)
		if err != nil {
			logz.Logger.Error("Unable to insert results",
				zap.Error(err),
				zap.String("insert", insert))
		}
		rows, err := res.RowsAffected()
		if err != nil {
			logz.Logger.Error("Unable to display Rows",
				zap.Error(err))
		}
		logz.Logger.Info("Rows from insert",
			zap.Int64("rows", rows),
			zap.String("insert", insert))
	} else {
		var id string
		err = data.Scan(&id)
		if err != nil {
			logz.Logger.Error("Unable to scan query results",
				zap.Error(err),
				zap.String("query", query))
		}
		logz.Logger.Info("current user id",
			zap.String("result", id))
	}
	logz.Logger.Info("jwt success result",
		zap.Any("sub", userMap["sub"]))
	err = data.Close()
	if err != nil {
		logz.Logger.Error("Unable to close rows connection",
			zap.Error(err))
	}
}

func (api *API) healthcheck(c echo.Context) error {
	data := health{
		Status: "UP",
	}
	return c.JSON(http.StatusOK, data)
}
